provider "aws" {
    region = "eu-west-1"
}

module "test_bucket_basic" {
    source = "../"

    bucket_name = "cirrius-test-bucket-basic"
    tags = {dummy_tag = "Dummy"}
}

module "test_bucket_kms" {
    source = "../"

    bucket_name = "cirrius-test-bucket-kms"
    tags = {dummy_tag = "Dummy"}
    sse_algorithm = "aws:kms"
    kms_master_key_arn = aws_kms_key.test_key.arn
}

resource "aws_kms_key" "test_key" {
    description             = "KMS key 1"
    deletion_window_in_days = 10
}

output "test_bucket_basic_name" {
    value = module.test_bucket_basic.s3_bucket.id
}

output "test_bucket_kms_name" {
    value = module.test_bucket_kms.s3_bucket.id
}