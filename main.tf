resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.bucket_name
  tags   = var.tags

  lifecycle {
    prevent_destroy = false
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {    
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kms_master_key_arn
        sse_algorithm     = var.sse_algorithm
      }
    }
  }
}
