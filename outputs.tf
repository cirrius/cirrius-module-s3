output "s3_bucket" {
  value       = aws_s3_bucket.s3_bucket
  description = "The s3 bucket object"
}
