/**
 * Terraform module to create S3 Buckets
 * See /examples/examples.tf for usage
 */

terraform {
    required_version = ">= 0.12.28"
    required_providers {
        aws = ">= 2.70"
    }
}