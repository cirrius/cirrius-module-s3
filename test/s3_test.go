package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestTerraformHelloWorldExample(t *testing.T) {
	terraformOptions := &terraform.Options{
		// Set the path to the Terraform code that will be tested.
		TerraformDir: "../examples",
	}

	// Clean up resources with "terraform destroy" at the end of the test.
	defer terraform.Destroy(t, terraformOptions)

	// Run "terraform init" and "terraform apply". Fail the test if there are any errors.
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the values of output variables and check they have the expected values.
	basicOutput := terraform.Output(t, terraformOptions, "test_bucket_basic_name")
	assert.Equal(t, "cirrius-test-bucket-basic", basicOutput)

	kmsOutput := terraform.Output(t, terraformOptions, "test_bucket_kms_name")
	assert.Equal(t, "cirrius-test-bucket-kms", kmsOutput)
}
